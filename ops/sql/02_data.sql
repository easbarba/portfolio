-- Portfolio is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Portfolio is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Portfolio. If not, see <https://www.gnu.org/licenses/>.

SET TIME ZONE 'America/Sao_Paulo';

-- SITES

INSERT INTO sites
       (id, title)
VALUES
      ((select UUID()), 'Pericles Bassols Pegado Cortez'),
      ((select UUID()), 'Raphael Claus'),
      ((select UUID()), 'Ricardo Marques Ribeiro'),
      ((select UUID()), 'Wilton Pereira Sampaio'),
      ((select UUID()), 'Anderson Daronco'),
      ((select UUID()), 'Marcelo Aparecido de Souza'),
      ((select UUID()), 'Marcelo de Lima Henrique'),
      ((select UUID()), 'Sandro Meira Ricci'),
      ((select UUID()), 'Rafael Traci'),
      ((select UUID()), 'Dewson Fernando Freitas da Silva');
