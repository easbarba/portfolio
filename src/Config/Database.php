<?php

declare(strict_types=1);

/*
* Portfolio is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Portfolio is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Portfolio. If not, see <https://www.gnu.org/licenses/>.
*/

namespace Easbarba\Portfolio\Config;

use PDO;

class Database
{
    public function __construct()
    {
    }

    public function connect(): void
    {
        $dsn = $_ENV["DATABASE_URL"];
        $pdo = new PDO($dsn, $_ENV["SQL_USERNAME"], $_ENV["SQL_PASSWORD"], [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);

        $stmt = $pdo->query("SELECT * FROM sites");
        $sites =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        print_r($sites);
    }
}
