# Portfolio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Portfolio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Portfolio. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include .env

NAME := portfolio
VERSION := $(shell cat .version)
RUNNER ?= podman
BACKEND_IMAGE=${CONTAINER_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ------------------------------------
base: initial database server

stats:
	${RUNNER} pod stats ${POD_NAME}

stop:
	${RUNNER} pod rm --force --ignore ${POD_NAME}
	${RUNNER} container rm --force --ignore ${NAME}
	${RUNNER} container rm --force --ignore ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}

initial:
	# ----------- CREATE POD
	${RUNNER} pod create \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--name ${POD_NAME}

database:
	# ----------- ADD DATABASE
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env MYSQL_ROOT_PASSWORD=${SQL_PASSWORD} \
		--env MYSQL_PASSWORD=${SQL_PASSWORD} \
		--env MYSQL_USER=${SQL_USERNAME} \
		--env MYSQL_DATABASE=${SQL_DATABASE} \
		--volume ${PWD}/ops/sql:/docker-entrypoint-initdb.d:Z \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

server:
	# ----------- SERVER
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--restart=unless-stopped \
		--name ${SERVER_NAME} \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--volume ./ops/nginx-default.conf:/etc/nginx/conf.d/default.conf:Z \
		${SERVER_IMAGE}

prod:
	# ----------- BACKEND PROD
	${RUNNER} run ${RUNNER_STATS} \
		--rm \
		--detach \
		--name ${NAME}-prod \
		--env-file ./envs/.env.db \
		${BACKEND_IMAGE}

start:
	# ----------- BACKEND DEV
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-start \
		--env-file ./.env \
		--env DATABASE_URL="${SQL_ENGINE}:host=${SQL_HOST};dbname=${SQL_DATABASE};port=${SQL_PORT};charset=utf8" \
		--env SQL_PASSWORD=${SQL_PASSWORD} \
		--env SQL_USERNAME=${SQL_USERNAME} \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c "php-fpm"

		# --volume ${PWD}/ops/php-fpm.conf:/usr/local/etc/php-fpm.d/zzz-docker.conf:Z \
repl:
	# ----------- BACKEND REPL
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-repl \
		--env-file ./envs/.env.db \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}/ops/php-fpm.conf:/usr/local/etc/php-fpm.d/zzz-docker.conf:Z \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash

repl-db:
	# ----------- DATABASE REPL
	podman exec --env MYSQL_PWD=${SQL_PASSWORD} -it ${DATABASE_NAME} ${SQL_REPL_CMD}

cmd:
	${RUNNER} run \
		--rm --tty --interactive \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c '$(shell cat symfony-commands | fzf)'

composer:
	${RUNNER} run \
		--rm --tty --interactive \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${COMPOSER_IMAGE} \
		bash -c '$(shell cat composer-commands | fzf)'

build:
	# ---------------------- BUILD BACKEND IMAGE
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${CONTAINER_REGISTRY}/${USER}/${NAME}:${VERSION}

publish:
	# ---------------------- PUBLISH BACKEND IMAGE
	${RUNNER} push ${BACKEND_IMAGE}

	# ---------------------- PUBLISH DATABASE IMAGE
	# ${RUNNER} commit ${DATABASE_NAME} ${CONTAINER_REGISTRY}/${USER}/${DATABASE_NAME}:${VERSION}
	# ${RUNNER} push ${CONTAINER_REGISTRY}/${USER}/${DATABASE_NAME}:${VERSION}

openapi:
	./vendor/bin/openapi src -o docs/openapi/${NAME}-openapi-${VERSION}.yaml

guix:
	guix shell --pure --container

.PHONY := base start stop prod server database cmd publish build repl repl-db stats openapi
.DEFAULT_GOAL := test
